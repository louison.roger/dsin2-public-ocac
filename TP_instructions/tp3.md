summary: TP3
id: tp3
categories: tp, api
tags: api, flask
status: Published
authors: OCTO Technology
Feedback Link: https://gitlab.com/octo-technology/octo-bda/cercle-formation/dsin2/-/issues/new

# TP3 - DevOps, Cloud et Infra-Code

## Overview
Duration: 20

Les manipulations seront faites par les formateurs pour ce TP.

Le formateur montre les différents éléments d'infrastructure déployés avec de l'infrastructure-as-code:

- Terraform,
- Ansible,
- Docker,
- Docker-compose